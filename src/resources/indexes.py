from flask_restful import Resource
from src.models.index import Index, IndexSchema


class Indexes(Resource):
  def get(self):
    """
    List of available indexes
    ---
    responses:
      500:
        description: Server error
      200:
        description: List of stock indexes
        schema:
          id: Index
          properties:
            index:
              type: string
              description: The index name
    """
    indexes = Index.get_all_indexes()
    indexes_schema = IndexSchema(many=True)
    result = indexes_schema.dump(indexes)
    return {'data': result}, 200
