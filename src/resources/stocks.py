from flask_restful import Resource
from src.models.stock import Stock, StockSchema


class Stocks(Resource):
  def get(self):
    """
    This endpoint is for getting all ticker names that can be used for getting the prices.
    ---
    responses:
      500:
        description: Server error
      200:
        description: List of stock names (tickers)
        schema:
          id: Stock
          properties:
            ticker:
              type: string
              description: The ticker name
            index:
              type: string
              description: The ticker's index
    """
    stocks = Stock.get_all_stocks()
    stocks_schema = StockSchema(many=True)
    result = stocks_schema.dump(stocks)
    return {'data': result}, 200
