from datetime import timedelta
from flask import Flask, request, abort
from flask_restful import Resource
from src.models.price import Price, PriceSchema, PriceQuerySchema


class Prices(Resource):
  def get(self):
    """
    This endpoint is for getting the prices of a given ticker starting from a date going back into specific period of time.
    ---
    parameters:
      - in: query
        name: ticker
        type: string
        required: true
        description: The ticker you want to get the prices of
      - in: query
        name: date
        type: string
        format: date
        description: The starting date in YYYY-MM-DD format
        default: 2018-12-31
      - in: query
        name: period
        type: integer
        description: The period (in days) for the prices data, 93 days maximum for this open API
        default: 7
    responses:
      500:
        description: Server error
      400:
        description: Ticker is empty, or period is larger than 3 months (93 days)
      200:
        description: List of resulting prices
        schema:
          id: Price
          properties:
            id:
              type: string
              description: ID of the ticker
            ticker:
              type: string
              description: The ticker name
            index:
              type: string
              description: The ticker's index
            date:
              type: string
              format: date
              description: The date
            open:
              type: number
              format: float
              description: Open price
            low:
              type: number
              format: float
              description: The lowest price
            high:
              type: number
              format: float
              description: The highest price
            close:
              type: number
              format: float
              description: The closing price
    """
    priceQuerySchema = PriceQuerySchema()
    errors = priceQuerySchema.validate(request.args)
    if errors:
        abort(400, str(errors))
    else:
      price_query = priceQuerySchema.load(request.args)
      ticker = price_query['ticker']
      dateBefore = str(price_query['date'] - timedelta(days=price_query['period']))
      dateEnd = str(price_query['date'])
      prices = Price.query.filter(Price.ticker == ticker).filter(Price.day.between(dateBefore, dateEnd)).all()
      prices_schema = PriceSchema(many=True)
      result = prices_schema.dump(prices)
      return {'data': result}, 200
