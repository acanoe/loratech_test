from flask import Flask
from flasgger import Swagger
from flask_marshmallow import Marshmallow
from flask_restful import Api
from src.config import create_config
from src.models import db
from src.resources.indexes import Indexes
from src.resources.prices import Prices
from src.resources.stocks import Stocks

app = Flask(__name__)

params = create_config()

# App config
# app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///../database.db"
app.config["SQLALCHEMY_DATABASE_URI"] = "postgres://{}:{}@{}/{}".format(
    params["user"],
    params["password"],
    params["host"],
    params["database"],
)
app.config["SQLALCHEMY_TRACK_MODIFICATION"] = False
app.config["SQLALCHEMY_ECHO"] = True
# App config for swagger
app.config["SWAGGER"] = {
  "title": "Lora Technologies Test API",
  "specs_route": "/"
}

# DB connection
db.init_app(app)
# db.create_all()

# Create api
api = Api(app)

# Swagger for documentation
template = {
  "swagger": "2.0",
  "info": {
    "title": "Lora Technologies Test API",
    "description": "API for my loratech_test project",
    "contact": {
      "name": "Hendika Nugroho",
      "url": "https://acanoe.github.io",
    },
    "version": "0.0.1",
    "schemes": [
      "http"
    ],
  },
}
swagger = Swagger(app, template=template)


# Create marshmallow instance
ma = Marshmallow(app)

# Add resources
api.add_resource(Indexes, "/indexes")
api.add_resource(Prices, "/prices")
api.add_resource(Stocks, "/stocks")
