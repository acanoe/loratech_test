from marshmallow import Schema, fields
from src.models import db
from src.models.stock import Stock, StockSchema


class Index(db.Model):
    __tablename__ = 'indexes'
    __table_args__ = {'schema' : 'loratech_test'}
    index = db.Column(db.Text, primary_key=True, nullable=False)

    @staticmethod
    def get_all_indexes():
        return Index.query.all()

    @staticmethod
    def get_one_index(id):
        return Index.query.get(id)


class IndexSchema(Schema):
    index = fields.Str(dump_only=True)