from datetime import date
from src.models import db
from marshmallow import Schema, fields, validate


class Price(db.Model):
    __tablename__ = 'prices'
    __table_args__ = {'schema': 'loratech_test'}
    id = db.Column(db.Integer, primary_key=True)
    ticker = db.Column(db.String(128), db.ForeignKey('loratech_test.tickers.ticker'))
    index = db.Column(db.Text, db.ForeignKey('loratech_test.indexes.index'))
    day = db.Column(db.Date, nullable=False)
    open = db.Column(db.Float, nullable=False)
    low = db.Column(db.Float, nullable=False)
    high = db.Column(db.Float, nullable=False)
    close = db.Column(db.Float, nullable=False)

    @staticmethod
    def get_one_price(id):
        return Price.query.get(id)


class PriceSchema(Schema):
    id = fields.Int(dump_only=True)
    ticker = fields.Str(dump_only=True)
    index = fields.Str(dump_only=True)
    day = fields.Date(dump_only=True)
    open = fields.Float(dump_only=True)
    low = fields.Float(dump_only=True)
    high = fields.Float(dump_only=True)
    close = fields.Float(dump_only=True)

    class Meta:
        ordered = True

class PriceQuerySchema(Schema):
    ticker = fields.Str(required=True)
    date = fields.Date(missing=date(2018, 12, 31))
    period = fields.Int(missing=7, validate=validate.Range(min=1, max=93))
