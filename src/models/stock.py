from src.models import db
from marshmallow import Schema, fields


class Stock(db.Model):
    __tablename__ = 'tickers'
    __table_args__ = {'schema': 'loratech_test'}
    ticker = db.Column(db.String(128), primary_key=True, nullable=False)
    index = db.Column(db.Text, db.ForeignKey('loratech_test.indexes.index'))

    @staticmethod
    def get_all_stocks():
        return Stock.query.all()

    @staticmethod
    def get_one_stock(id):
        return Stock.query.get(id)


class StockSchema(Schema):
    ticker = fields.Str(dump_only=True)
    index = fields.Str(dump_only=True)
