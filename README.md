# Lora Technologies Application Test

## Configuration
Before starting the server, create `database.ini` file inside `src/config` folder with the following data:
```
[postgresql]
host=<your host>
database=<databse name>
user=<username>
password=<password>
```

## Starting the app
`cd` into the folder and then run `python run.py` or:
```
$ export FLASK_APP=run.py
$ export FLASK_ENV=development
$ flask run
```

## Documentation
Swagger page is available in `/`. e.g. http://localhost:3000/

## Requirements
Required modules are in the `requirements.txt` file