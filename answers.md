# SQL questions

## First Question
### Question

```
For each market (index) for each month during the 11 year period, 2008-2018 (2008-01 to 2018-12), please calculate the average price of all the stocks in each market/index for that for each month, i.e. the average price of the stock in a market/index over the calendar month
```

### Answer
```
select
  "index",
  date_trunc('month', day) as month,
  avg("close") as average
from
  loratech_test.prices
where
  "day" between '2008-01-01' and '2018-12-31 23:59:59'
group by
  1, 2
order by
  1, 2;
```

## Second Question
### Question

```
Only among HK stocks, for each month during the 11 year period, 2008-2018, calculate the maximum, minimum, and average stock price. For only among KOSPI2 stocks, for each month during the 11 year period, 2008-2018, please find the fifth highest and lowest price
```
### Answer
#### First part
```
select
  "index" ,
  date_trunc('month', day) as month,
  max("close") as maximum,
  min("close") as minimum,
  avg("close") as average
from
  loratech_test.prices
where
  "day" between '2008-01-01' and '2018-12-31 23:59:59'
  and "index" like 'HK'
group by
  1, 2
order by
  1, 2;
```
#### Second part
```
I cant seem to find the answer, sorry.
```